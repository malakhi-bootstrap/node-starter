# node-starter
This is a bootstrap repository for my projects written in Node.js. It includes basic eslint configs and a .gitignore. The settings reflect my opinions. Feel free to fork and make PRs, but this is mostly for my use.
